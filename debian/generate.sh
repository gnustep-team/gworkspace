#!/bin/sh
base_dir=`pwd`
g_app="debian/gworkspace.app"
GNUSTEP_SYSTEM_APPS="usr/lib/GNUstep/Applications"
GNUSTEP_SYSTEM_BUNDLES="usr/lib/GNUstep/Bundles"
GNUSTEP_SYSTEM_FRAMEWORKS="usr/lib/GNUstep/Frameworks"
GNUSTEP_SYSTEM_SERVICES="usr/lib/GNUstep/Services"
usrshare="usr/share/GNUstep"
usrinclude="usr/include/GNUstep"

cat <<EOF >$base_dir/debian/gworkspace.app.install.temp
debian/GWorkspace.desktop	/usr/share/applications
GWorkspace.xpm		usr/share/pixmaps
usr/bin/GWorkspace
usr/bin/Recycler
usr/bin/ddbd
usr/bin/fswatcher
usr/bin/lsf*
usr/bin/resizer
usr/bin/search*
usr/bin/wopen
usr/include
usr/lib/lib*
EOF

cat <<EOF >$base_dir/debian/mdfinder.app.install.temp
MDFinder.xpm		usr/share/pixmaps
usr/bin/MDFinder
usr/bin/gmds
usr/bin/md*
EOF


rm -f $base_dir/debian/gworkspace-common.install.temp
rm -f $base_dir/debian/gworkspace.app.links.temp
rm -f $base_dir/debian/mdfinder.app.links.temp
rm -f $base_dir/debian/mdfinder.app.maintscript.temp
rm -f $base_dir/debian/gworkspace.app.maintscript.temp
rm -f $base_dir/debian/mdfinder.app.maintscript.temp

append()
{
  arch_dep_package="$base_dir/debian/$1.app"
  arch_indep_package="$base_dir/debian/$1-common"
  tmp_dir=$base_dir/debian/tmp
  usrlib=$2
  args=$3
  for dir in `ls -A --color=never $args $tmp_dir/$usrlib`
  do
    (echo "$usrlib/$dir/Resources/*\t$usrshare/$dir") >>$arch_indep_package.install.temp
    (echo "$usrshare/$dir\t $usrlib/$dir/Resources") >>$arch_dep_package.links.temp
    (echo "dir_to_symlink\t/$usrlib/$dir/Resources\t/$usrshare/$dir\t0.9.3-1~") >>$arch_dep_package.maintscript.temp
    for i in `ls -A --color=never --ignore="Resources" --ignore="stamp.make" $tmp_dir/$usrlib/$dir`
    do
      (echo "$usrlib/$dir/$i") >>$arch_dep_package.install.temp
    done
  done
  
}

append_for_frameworks()
{
    for i in `ls -A --color=never $base_dir/debian/tmp/$1`
    do
	if [ $i = Resources ]
	then
	    str=`echo $1 | cut -d/ -f5`
	    (echo "$usrshare/Frameworks/$str\t$1/$i") >>$base_dir/debian/gworkspace.app.links.temp
	    (echo "$1/$i/*\t$usrshare/Frameworks/$str") >>$base_dir/debian/gworkspace-common.install.temp
	    (echo "dir_to_symlink\t/$1/$i\t/$usrshare/Frameworks/$str\t0.9.3-1~") >>$base_dir/debian/gworkspace.app.maintscript.temp
	elif test -L $base_dir/debian/tmp/$1/$i
	then
	    (echo $1/$i) >>$base_dir/debian/gworkspace.app.install.temp
	elif [ $i = Headers ]
	then
	    str=`echo $1 | cut -d/ -f5`
	    version=`echo $1 | cut -d/ -f7`
	    (echo "$usrinclude/Frameworks/$str/Versions/$version\t$1/$i") >>$base_dir/debian/gworkspace.app.links.temp
	    (echo "$1/$i/*\t$usrinclude/Frameworks/$str/Versions/$version") >>$base_dir/debian/gworkspace.app.install.temp
	    (echo "dir_to_symlink\t/$1/$i\t/$usrinclude/Frameworks/$str/Versions/$version\t0.9.3-1~") >>$base_dir/debian/gworkspace.app.maintscript.temp
	elif test -f $base_dir/debian/tmp/$1/$i
	then
	    (echo $1/$i) >>$base_dir/debian/gworkspace.app.install.temp
	elif test -d $base_dir/debian/tmp/$1/$i
	then
	    append_for_frameworks $1/$i
	fi
    done
}

# 1 - Install all Resources dirs to /usr/share/GNUstep in gwokspace-common package
#     and symlink them from /usr/lib/GNUstep subdirs,
#     move Header files to /usr/include/ and symlink then from /usr/lib/GNUstep subdirs,
#      install other files in /usr/lib/GNUstep subdirs.

# 1.1 - Applications except MDFinder.app
append gworkspace $GNUSTEP_SYSTEM_APPS "--ignore=MDFinder.app"

# 1.2 - Bundles except Extractors and MDIndexing 
append gworkspace $GNUSTEP_SYSTEM_BUNDLES "--ignore=*.extr --ignore=MDIndexing.prefPane"

#1.3 - Services
append gworkspace $GNUSTEP_SYSTEM_SERVICES

# 1.4 - Frameworks
append_for_frameworks $GNUSTEP_SYSTEM_FRAMEWORKS


# 2 - Same job for mdfinder.app

# 2.1 - Application
for dir in "MDFinder.app"
do
    (echo "$GNUSTEP_SYSTEM_APPS/$dir/Resources/*\t$usrshare/$dir") >>$base_dir/debian/mdfinder.app.install.temp
    (echo "$usrshare/$dir\t $GNUSTEP_SYSTEM_APPS/$dir/Resources") >>$base_dir/debian/mdfinder.app.links.temp
    (echo "dir_to_symlink\t/$GNUSTEP_SYSTEM_APPS/$dir/Resources\t/$usrshare/$dir\t0.9.3-1~") >>$base_dir/debian/mdfinder.app.maintscript.temp
    for j in `ls -A --color=never --ignore="Resources" --ignore="stamp.make" $base_dir/debian/tmp/$GNUSTEP_SYSTEM_APPS/$dir`
    do
	(echo "$GNUSTEP_SYSTEM_APPS/$dir/$j") >>$base_dir/debian/mdfinder.app.install.temp
    done
done



# 2.2 - Bundles

# 2.2.1 - PrefPanes
for dir in `ls -A --color=never $base_dir/debian/tmp/$GNUSTEP_SYSTEM_BUNDLES/`
do
    case $dir in
	*prefPane)
	    (echo "$GNUSTEP_SYSTEM_BUNDLES/$dir/Resources/*\t$usrshare/$dir") >>$base_dir/debian/mdfinder.app.install.temp
	    (echo "$usrshare/$dir\t $GNUSTEP_SYSTEM_BUNDLES/$dir/Resources") >>$base_dir/debian/mdfinder.app.links.temp
	    (echo "dir_to_symlink\t/$GNUSTEP_SYSTEM_BUNDLES/$dir/Resources\t/$usrshare/$dir\t0.9.3-1~") >>$base_dir/debian/mdfinder.app.maintscript.temp
	    for file in `ls -A --color=never --ignore="Resources" --ignore="stamp.make" $base_dir/debian/tmp/$GNUSTEP_SYSTEM_BUNDLES/$dir`
	    do
		(echo "$GNUSTEP_SYSTEM_BUNDLES/$dir/$file") >>$base_dir/debian/mdfinder.app.install.temp
	    done
	    ;;
    esac
done

# 2.2.2 - Extractors
for dir in `ls -A --color=never $base_dir/debian/tmp/$GNUSTEP_SYSTEM_BUNDLES/`
do
    case $dir in
	*extr)
	    (echo "$GNUSTEP_SYSTEM_BUNDLES/$dir/Resources/*\t$usrshare/$dir") >>$base_dir/debian/mdfinder.app.install.temp
	    (echo "$usrshare/$dir\t $GNUSTEP_SYSTEM_BUNDLES/$dir/Resources") >>$base_dir/debian/mdfinder.app.links.temp
	    (echo "dir_to_symlink\t/$GNUSTEP_SYSTEM_BUNDLES/$dir/Resources\t/$usrshare/$dir\t0.9.3-1~") >>$base_dir/debian/mdfinder.app.maintscript.temp
	    for j in `ls -A --color=never --ignore="Resources" --ignore="stamp.make" $base_dir/debian/tmp/$GNUSTEP_SYSTEM_BUNDLES/$dir`
	    do
		(echo "$GNUSTEP_SYSTEM_BUNDLES/$dir/$j") >>$base_dir/debian/mdfinder.app.install.temp
	    done
    esac
done

mv $base_dir/debian/gworkspace-common.install.temp	$base_dir/debian/gworkspace-common.install
mv $base_dir/debian/gworkspace.app.install.temp		$base_dir/debian/gworkspace.app.install
mv $base_dir/debian/gworkspace.app.links.temp		$base_dir/debian/gworkspace.app.links
mv $base_dir/debian/gworkspace.app.maintscript.temp	$base_dir/debian/gworkspace.app.maintscript
mv $base_dir/debian/mdfinder.app.maintscript.temp	$base_dir/debian/mdfinder.app.maintscript
mv $base_dir/debian/mdfinder.app.install.temp		$base_dir/debian/mdfinder.app.install
mv $base_dir/debian/mdfinder.app.links.temp		$base_dir/debian/mdfinder.app.links
