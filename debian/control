Source: gworkspace
Section: gnustep
Priority: optional
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders:
 Eric Heintzmann <heintzmann.eric@free.fr>,
 Jonathan Shipley <jon@windj.ath.cx>,
 Alex Myczko <tar@debian.org>,
 Yavor Doganov <yavor@gnu.org>,
Build-Depends:
 debhelper-compat (= 13),
 imagemagick,
 libgnustep-gui-dev (>= 0.32),
 libpopplerkit-dev,
 libpreferencepanes-dev (>= 1.2.1-2),
 libsqlite3-dev,
Rules-Requires-Root: no
Standards-Version: 4.7.2
Vcs-Git: https://salsa.debian.org/gnustep-team/gworkspace.git
Vcs-Browser: https://salsa.debian.org/gnustep-team/gworkspace
Homepage: http://www.gnustep.org/experience/GWorkspace.html

Package: gworkspace.app
Architecture: any
Depends:
 gworkspace-common (= ${source:Version}),
 gworkspace-libs (= ${binary:Version}),
 libinspector1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 addresses-goodies-for-gnustep,
 cynthiune.app,
 preview.app | price.app,
 systempreferences.app,
 textedit.app,
 viewpdf.app,
 zipper.app,
Suggests:
 gworkspace-apps-wrappers,
Description: GNUstep Workspace Manager
 GWorkspace is the official GNUstep workspace manager. It is a clone
 of NeXT's workspace manager. GWorkspace is probably one of the most
 useful and usable workspace managers available on any platform,
 owing to its well-designed interface and the natural, consistent
 design that it inherits from the GNUstep framework.

Package: gworkspace-common
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Recommends:
 gworkspace.app,
Description: GNUstep Workspace Manager - common files
 GWorkspace is the official GNUstep workspace manager.
 .
 This package contains the architecture-independent files; it is not
 useful on its own.

Package: gworkspace-libs
Architecture: any
Multi-Arch: same
Depends:
 gworkspace-common (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gworkspace.app,
Breaks:
 gworkspace.app (<< 1.0.0-3),
Replaces:
 gworkspace.app (<< 1.0.0-3),
Description: GNUstep Workspace Manager - core libraries
 GWorkspace is the official GNUstep workspace manager.
 .
 This package contains the GWorkspace shared librariesl it is not
 useful on its own.

Package: libinspector-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends:
 libgnustep-gui-dev,
 libinspector1 (= ${binary:Version}),
 ${misc:Depends},
Breaks:
 gworkspace.app (<< 1.0.0-3),
Replaces:
 gworkspace.app (<< 1.0.0-3),
Description: GWorkspace inspector framework (development files)
 GWorkspace is the official GNUstep workspace manager.  The Inspector
 framework is a core part of it allowing specialized inspection of
 files with different filetypes.
 .
 This package contains the headers and .so symlink of the GWorkspace
 Inspector framework needed to develop inspectors for GWorkspace.

Package: libinspector1
Architecture: any
Section: libs
Multi-Arch: same
Depends:
 gworkspace-libs (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gworkspace.app,
Breaks:
 gworkspace.app (<< 1.0.0-3),
Replaces:
 gworkspace.app (<< 1.0.0-3),
Description: GWorkspace inspector framework (shared library)
 GWorkspace is the official GNUstep workspace manager.  The Inspector
 framework is a core part of it allowing specialized inspection of
 files with different filetypes.
 .
 This package contains the shared library.

Package: mdfinder.app
Architecture: any
Depends:
 gworkspace-libs (= ${binary:Version}),
 unzip,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 systempreferences.app,
Description: GNUstep Finder and system-wide search system
 MDFinder is a an application to search items already indexed by the
 GWMetadata indexing system, a GNUstep implementation of MacOS X's
 proprietary Spotlight.  Searches can be saved and are automatically
 updated if the corresponding files change.  Indexing is done in the
 background and is switched off by default.  To enable it and
 configure the search path, use the dedicated module in
 the SystemPreferences application.

Package: gworkspace-apps-wrappers
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
Recommends:
 gworkspace.app,
 wrapperfactory.app,
Suggests:
 abiword,
 alsa-utils,
 bluefish,
 calibre,
 coccinella,
 emacs,
 firefox-esr | firefox,
 gedit,
 gimp,
 gnumeric,
 imagemagick,
 konqueror,
 lyx,
 nedit,
 sox,
 vim | nvi,
 xchat,
 xfig,
 xine-ui,
 xpdf,
Description: Application wrappers for GWorkspace
 A few wrapper scripts to enable usage of non-GNUstep applications
 within a GNUstep environment.
 .
 More wrappers can be made with wrapperfactory.app.
